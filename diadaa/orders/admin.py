from django.contrib import admin
from .models import BulkDemand, Order, OrderItem


class BulkDemandAdmin(admin.ModelAdmin):
    list_display = ('user', 'produce', 'quantity', 'price',
                    'start_date', 'address', 'digital_address')
    list_filter = ('paid', 'timestamp')


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    raw_id_fields = ['produce']


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'address', 'city', 'paid')
    list_filter = ('paid', 'timestamp', 'updated')
    inlines = (OrderItemInline,)


admin.site.register(BulkDemand, BulkDemandAdmin)
admin.site.register(Order, OrderAdmin)
