from django.conf import settings
from django.core.mail import send_mail
# from django_celery_results.backends import DatabaseBackend

from diadaa.taskapp.celery import app


@app.task(name='Order created', bind=True, default_retry_delay=300, max_retries=5)
def order_created(self, order_id):
    """
    Task to send e-mail notification when an order is successfully created.
    :param order_id: Order id of created order
    :return: mail sent object
    """
    from diadaa.orders.models import Order

    order = Order.objects.get(id=order_id)
    subject = 'Order #{}'.format(order_id)
    message = 'Dear {},\n\nYou have successfully placed an order. ' \
              'Your order id is #{}.'.format(order.first_name, order_id)
    mail_sent = send_mail(subject=subject,
                          message=message,
                          from_email='admin@diadaa.com',
                          recipient_list=[settings.EMAIL_HOST, order.email],
                          fail_silently=True)
    return mail_sent
