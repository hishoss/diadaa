from django.db import models
from django.db.models import Q

from diadaa.produce.models import ProduceCollection
from diadaa.users.models import User


class BulkDemand(models.Model):
    PERIODS_CHOICES = (
        ('d', 'Daily'),
        ('w', 'Weekly'),
        ('m', 'Monthly'),
        ('o', 'Other'),
    )
    user = models.ForeignKey(User)
    produce = models.ForeignKey(ProduceCollection,
                                # limit_choices_to=Q(user__username='diadaa'),
                                related_name='bulk_items')
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField(default=1)
    units = models.CharField(max_length=20, verbose_name='Units for quantity')
    periods_needed = models.CharField(max_length=1, choices=PERIODS_CHOICES)
    other = models.CharField(max_length=100, blank=True)
    start_date = models.DateField()  # also need an interval
    # duration = models.DurationField(default=timedelta())
    address = models.CharField(max_length=250)
    digital_address = models.CharField(max_length=20, blank=True)  # include in member model
    comment = models.TextField(blank=True, verbose_name='Further information')
    paid = models.BooleanField(default=False)
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-timestamp',)

    def __str__(self):
        return 'Order {}'.format(self.id)

    @property
    def get_cost(self):
        return self.price * self.quantity


class Order(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField()
    address = models.CharField(max_length=250)
    postal_code = models.CharField(max_length=20)
    city = models.CharField(max_length=100)
    paid = models.BooleanField(default=False)
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-timestamp',)

    def __str__(self):
        return 'Order {}'.format(self.id)

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.items.all())

    @property
    def name(self):
        return self.first_name + ' ' + self.last_name


class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='items')
    produce = models.ForeignKey(ProduceCollection, related_name='order_items')
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return '{}'.format(self.id)

    def get_cost(self):
        return self.price * self.quantity
