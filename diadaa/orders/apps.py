from django.apps import AppConfig


class OrdersConfig(AppConfig):
    name = 'diadaa.orders'
    verbose_name = 'Orders'
