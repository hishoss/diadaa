from django import forms
from django.forms.extras.widgets import SelectDateWidget
from .models import Order, BulkDemand


class OrderCreateForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ('first_name', 'last_name', 'email', 'address', 'postal_code', 'city')


class BulkDemandForm(forms.ModelForm):
    class Meta:
        model = BulkDemand
        fields = ('produce', 'quantity', 'start_date', 'periods_needed',
                  'other', 'address', 'digital_address', 'comment')
        widgets = {'start_date': SelectDateWidget()}
