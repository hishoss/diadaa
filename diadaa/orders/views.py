from django.shortcuts import render
from diadaa.cart.cart import Cart

from .forms import OrderCreateForm, BulkDemandForm
from .models import OrderItem
from .tasks import order_created


def order_create(request):
    cart = Cart(request)
    form = OrderCreateForm(request.POST or None)
    if form.is_valid():
        order = form.save()
        for item in cart:
            OrderItem.objects.create(order=order,
                                     produce=item['produce'],
                                     price=item['price'],
                                     quantity=item['quantity'])
        cart.clear()
        # launch asynchronous task
        order_created.delay(order.id)
        return render(request, 'orders/order_created.html', {'order': order})
    return render(request, 'orders/order_create.html', {'cart': cart, 'form': form})


# @require_POST
def bulk_demand_create(request):
    form = BulkDemandForm(request.POST or None)
    if form.is_valid():
        demand_obj = form.save(commit=False)
        demand_obj.user = request.user
        demand_obj.price = demand_obj.produce.price
        demand_obj.save()
        return render(request, 'orders/order_created.html', {'order': demand_obj})
    return render(request, 'orders/bulk_demand_create.html', {'form': form})
