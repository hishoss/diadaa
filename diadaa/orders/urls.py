from django.conf.urls import url
from . import views


urlpatterns = (
    url(r'^bulk/$', views.bulk_demand_create, name='bulk_demand_create'),
    url(r'^create/$', views.order_create, name='order_create'),
)
