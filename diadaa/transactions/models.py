from django.db import models
from django.db.models import Q
from django.utils import timezone

from diadaa.users.models import Member
# from diadaa.produce.models import Produce, Transport, Carrier
from .services import goods_before_departure, goods_after_arrival


class Transaction(models.Model):
    STATUS_CHOICES = (
        ('P', 'Pending'),
        ('D', 'Declined'),
        ('D', 'Success'),
    )
    buyer = models.ForeignKey(Member,
                              limit_choices_to=Q(is_consumer=True) | Q(is_retailer=True),
                              related_name='buyer_transactions', blank=False)
    farmer = models.ForeignKey(Member,
                               limit_choices_to=Q(is_farmer=True),
                               related_name='farmer_transactions',  blank=False)
    agent = models.ForeignKey(Member,
                              limit_choices_to=Q(is_agent=True),
                              related_name='agent_transactions', blank=False)
    # produce = models.ForeignKey(Produce)
    pogbd = models.ImageField(verbose_name="picture of goods before departure",
                              upload_to=goods_before_departure, default='', blank=False)
    pogaa = models.ImageField(verbose_name="picture of goods after arrival",
                              upload_to=goods_after_arrival, default='', blank=True)
    quantity = models.FloatField()
    # limit_choices_to=Q(capacity__lte=quantity))
    # transport = models.ForeignKey(Transport, verbose_name='Boarding vehicle')
    # carrier = models.ForeignKey(Carrier)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default='P')
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "{} purchased {} from {}".format(self.buyer.user.username,
                                                self.produce,
                                                self.farmer.user.username)

    class Meta:
        ordering = ('-updated',)
