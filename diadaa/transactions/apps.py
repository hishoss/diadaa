from django.apps import AppConfig


class TransactionsConfig(AppConfig):
    name = 'diadaa.transactions'
    verbose_name = 'Transactions'
