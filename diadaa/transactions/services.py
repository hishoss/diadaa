def user_image_path(instance, filename):
    return 'users/{0}'.format(instance.pk)


def goods_before_departure(instance, filename):
    return 'transaction_id_{0}/{1}'.format(instance.pk,instance.pogbd)


def goods_after_arrival(instance, filename):
    return 'transaction_id_{0}/{1}'.format(instance.pk,instance.pogaa)


def vehicle_directory_path(instance, image):
    return 'vehicle/{0}'.format(instance.vehicle_number)
