from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.shortcuts import render, redirect
from .forms import ApplyForm, ContactForm


def apply_view(request):
    form = ApplyForm(request.POST or None)
    if form.is_valid():
        cd = form.cleaned_data
        send_mail(subject='application as an agent for {}'.format(request.user.username),
                  message=cd['message'] + '\nnational_id:{0}\ndigital_address: {1}'.format(
                      cd['national_id'], cd['digital_address']),
                  from_email=request.user.email,
                  recipient_list=[settings.EMAIL_HOST],
                  fail_silently=True)
        messages.info(request, "Thanks for applying, we'll contact you shortly" )
        return redirect(request.user)
    return render(request, 'pages/contact.html', {'form': form})


def contact_view(request):
    form = ContactForm(request.POST or None)
    if form.is_valid():
        cd = form.cleaned_data
        send_mail(subject='contact from {0} on diadaa'.format(cd['email']),
                  message=cd['message'],
                  from_email=cd['email'],
                  recipient_list=[settings.EMAIL_HOST],
                  fail_silently=True)
        messages.info(request, 'Thanks for contacting us')
        return redirect('home')
    return render(request, 'pages/contact.html', {'form': form})

