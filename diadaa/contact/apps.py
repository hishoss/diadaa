from django.apps import AppConfig


class ContactConfig(AppConfig):
    name = 'diadaa.contact'
    verbose_name = 'Contact'
