from django import forms


class ApplyForm(forms.Form):
    national_id = forms.CharField(max_length=20)
    digital_address = forms.CharField(max_length=20)
    message = forms.CharField(widget=forms.Textarea)  # , verbose_name='Reasons you want to be an agent')


class ContactForm(forms.Form):
    name = forms.CharField(max_length=100, required=False)
    email = forms.EmailField()
    message = forms.CharField(widget=forms.Textarea)
