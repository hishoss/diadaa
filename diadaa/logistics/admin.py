from django.contrib import admin
from .models import Carrier, Inventory, Transport


class InventoryAdmin(admin.ModelAdmin):
    list_display = ('produce', 'stock', 'total_demand', 'demand_locations')

    def has_add_permission(self, request):
        return False


admin.site.register(Carrier)
admin.site.register(Inventory, InventoryAdmin)
admin.site.register(Transport)
