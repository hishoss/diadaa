from django import forms
from django.forms.extras.widgets import SelectDateWidget
from .models import Transport


class TransportForm(forms.ModelForm):
    class Meta:
        model = Transport
        fields = ('vehicle_number', 'departure_region', 'departure_location', #'time_of_departure',
                  'arrival_region', 'arrival_location', 'eta', 'transport_route', 'available_stops',
                  'image', 'capacity', 'availability')
        widgets = {'eta': SelectDateWidget(),
                   'availability': SelectDateWidget()}
