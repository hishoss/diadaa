from django.apps import AppConfig


class LogisticsConfig(AppConfig):
    name = 'diadaa.logistics'
    verbose_name = 'Logistics'
