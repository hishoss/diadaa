from django.views.generic import DetailView, ListView
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.mixins import LoginRequiredMixin

from .forms import TransportForm
from .models import Carrier, Transport


class TransportListView(LoginRequiredMixin, ListView):
    model = Transport
    # template_name = 'logistics/transport_list.html'
    context_object_name = 'transports'


def transport_detail_view(request, id=None):
    template_name = 'logistics/transport_list.html'
    transport = get_object_or_404(Transport, id=id)
    # object_viewed_signal.send(Member, instance=transport, request=request)
    return render(request, template_name, {'member': transport})


class TransportDetailView(LoginRequiredMixin, DetailView):
    model = Transport
    slug_field = 'vehicle_number'
    slug_url_kwarg = 'vehicle_number'


# def transport_create_view(request):
#     form = TransportForm(request.POST or None)
#     if form.is_valid():
#         produce_obj = form.save(commit=False)
#         produce_obj.owner = request.user
#         produce_obj.slug = slugify(produce_obj.name, request.user.id)
#         # quick fix. Need a way to use a date picker
#         # checkout https://github.com/pbucher/django-bootstrap-datepicker
#         produce_obj.date_from_source = timezone.now().date()
#         produce_obj.availability = timezone.now().date()
#         produce_obj.save()
#         return HttpResponseRedirect('/members/{id}'.format(id=request.user.id))
#     return render(request, 'farm_to_buyer/produce_create.html', {'form': form})

