from django.conf.urls import url
from . import views


urlpatterns = [
    # url(r'^carrier/$', views.TransportListView.as_view(), name='carrier_list'),
    url(r'^transport/$', views.TransportListView.as_view(), name='transport_list'),
    url(r'^transport/(?P<vehicle_number>[-\w]+/?)$', views.TransportDetailView.as_view(), name='transport_detail'),
]
