from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver

from diadaa.produce.models import Produce

from .services import vehicle_directory_path


# class BusStation(models.Model):
#     pass


class Carrier(models.Model):
    CARRIER_CHOICE = (
        ('L', 'Individual'),
        ('M', 'Private vehicle'),
        ('H', 'Bus driver'),
    )
    name = models.CharField(max_length=100)
    carrier_type = models.CharField(max_length=1, choices=CARRIER_CHOICE, default='L', blank=False)
    carrier_phone = models.CharField(max_length=17)
    description_of_goods = models.TextField(default='')
    agree_to_terms = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Inventory(models.Model):
    # change this to diaDaaInventory
    produce = models.ForeignKey(Produce,
                                limit_choices_to=Q(user__username='diadaa'),
                                on_delete=models.CASCADE)

    def __str__(self):
        return self.produce.name

    @property
    def demand_locations(self):
        # TODO: demand isn't always from bulk demand
        return ", ".join([produce.user.member.get_region_display() for produce in self.produce.collection.bulk_items.all()])

    @property
    def stock(self):
        return self.produce.total_stock

    @property
    def total_demand(self):
        # TODO: demand isn't always from bulk demand
        return sum([produce.quantity for produce in self.produce.collection.bulk_items.all()])

    def get_absolute_url(self):
        # TODO: give staff/agent a view outside backend to see what is needed
        pass

    class Meta:
        verbose_name_plural = 'Inventories'


@receiver(post_save, sender=Produce)
def create_inventory_item(**kwargs):
    if kwargs['created'] and kwargs['instance'].user.username == 'diadaa':
    # and instance.name or instance.collection.name isn't in Inventory objects
            Inventory.objects.create(produce=kwargs['instance'])


class Transport(models.Model):
    REGION_CHOICES = (
      ('ar', 'Ashanti Region'),
      ('ba', 'Brong Ahafo Region'),
      ('cr', 'Central Region'),
      ('er', 'Eastern Region'),
      ('ga', 'Greater Accra'),
      ('nr', 'Northern Region'),
      ('ue', 'Upper East Region'),
      ('uw', 'Upper West Region'),
      ('vr', 'Volta Region'),
      ('wr', 'Western Region'),
    )
    vehicle_number = models.CharField(max_length=20, blank=False)
    name_of_driver = models.CharField(max_length=100)
    departure_region = models.CharField(max_length=3,  choices=REGION_CHOICES)
    departure_location = models.CharField(max_length=100)
    time_of_departure = models.DateTimeField()
    arrival_region = models.CharField(max_length=3,  choices=REGION_CHOICES)
    arrival_location = models.CharField(max_length=100)
    eta = models.TimeField(verbose_name='estimated time of arrival')
    transport_route = models.CharField(max_length=200)
    available_stops = models.CharField(max_length=200)
    image = models.ImageField(upload_to=vehicle_directory_path, default='')
    capacity = models.FloatField(blank=False)
    # cost_per_weight
    availability = models.DateTimeField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.vehicle_number

    def get_absolute_url(self):
        return reverse('logistics:transport_detail', kwargs={'vehicle_number': self.vehicle_number})
