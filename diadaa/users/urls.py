from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.MemberTypeListView.as_view(), name='member_type_list'),
    url(r'^~redirect/$', views.UserRedirectView.as_view(), name='redirect'),
    url(r'^~update/$', views.UserUpdateView.as_view(), name='update'),
    url(r'^all/$', views.MemberListView.as_view(), name='member_list'),
    url(r'^agents/$', views.AgentListView.as_view(), name='agent_list'),
    url(r'^consumers/$', views.ConsumerListView.as_view(), name='consumer_list'),
    url(r'^farmers/$', views.FarmerListView.as_view(), name='farmer_list'),
    url(r'^retailers/$', views.RetailerListView.as_view(), name='retailer_list'),
    url(r'^wholesalers/$', views.WholesalerListView.as_view(), name='wholesaler_list'),
    url(r'^(?P<username>[\w.@+-]+)/$', views.UserDetailView.as_view(), name='user_detail'),
    url(r'^~(?P<username>[\w.@+-]+)/$', views.member_detail_view, name='member_detail'),
]
