def member_image_path(instance, filename):
    return 'member/{0}'.format(instance.user.username)
