from django.db import models


class MemberQuerySet(models.QuerySet):
    def _active(self):
        return self.filter(is_active=True)

    def agents(self):
        return self._active().filter(is_agent=True)

    def consumers(self):
        return self._active().filter(is_consumer=True)

    def farmers(self):
        return self._active().filter(is_farmer=True)

    def retailers(self):
        return self._active().filter(is_retailer=True)

    def wholesalers(self):
        return self._active().filter(is_wholesaler=True)


# If no complex queries are required, just use MemberQuerySet.as_manager()
class MemberManager(models.Manager):
    def get_queryset(self):
        return MemberQuerySet(self.model, using=self._db)

    def agents(self):
        return self.get_queryset().agents()

    def consumers(self):
        return self.get_queryset().consumers()

    def farmers(self):
        return self.get_queryset().farmers()

    def retailers(self):
        return self.get_queryset().retailers()

    def wholesaler(self):
        return self.get_queryset().wholesalers()
