from django.contrib.auth.mixins import LoginRequiredMixin
# from django.contrib import messages
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic import DetailView, ListView, RedirectView, UpdateView

from diadaa.analytics.recommendation_engines import produce_recommendation_list
from diadaa.produce.models import Produce

from .models import User, Member


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'

    def get_context_data(self, **kwargs):
        context = super(UserDetailView, self).get_context_data(**kwargs)
        context['produce'] = produce_recommendation_list(self.request.user.id)
        return context

    # def dispatch(self, request, *args, **kwargs):
    #     if request.user.member.is_new_member:
    #         messages.info(request, 'Please update your information')
    #         return redirect('users:update')
    #     else:
    #         super(UserDetailView, self).dispatch(request, *args, **kwargs)


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse('users:user_detail',
                       kwargs={'username': self.request.user.username})


class UserUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'users/user_form.html'

    fields = [
        'secondary_phone', 'region', 'location',
        'is_consumer', 'is_farmer', 'is_retailer'
    ]
    model = Member

    def get_object(self):
        # Only get the Member record for the user making the request
        return Member.members.get(user=self.request.user.id)

    # send the user back to their own page after a successful update
    def get_success_url(self):
        return reverse('users:user_detail',
                       kwargs={'username': self.request.user.username})


class UserListView(LoginRequiredMixin, ListView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'


# def member_detail_view(request, id=None):
#     template = 'users/member_detail.html'
#     member = get_object_or_404(Member, id=id)
#     # object_viewed_signal.send(Member, instance=member, request=request)
#     return render(request, template, {'member': member})


# class MemberDetailView(DetailView, ObjectViewedMixin):
#     queryset = Member.members.all()
#     template_name = 'produce/member_detail.html'
#
#     def get_object(self, **kwargs):
#         return get_object_or_404(Member, id=self.kwargs.get('id', None))


# class MemberDetailView(LoginRequiredMixin, DetailView):
#     query_set = Produce
#     template_name = 'produce/produce_list.html'
#     # These next two lines tell the view to index lookups by username
#     slug_field = 'user__username'
#     slug_url_kwarg = 'username'
#
#     # def get_queryset(self):
#     #     return Produce.objects.filter(user=self.request.user)
#
#     def get_context_data(self, **kwargs):
#         context = super(MemberDetailView).get_context_data(**kwargs)
#         context['produce'] = Produce.objects.filter(user=self.request.user)
#         return context

def member_detail_view(request, username):
    context = {'produce': Produce.objects.filter(user__username=username)}
    template = 'users/member_detail.html'
    return render(request, template, context)


def member_list_view(request):
    members = Member.members.all()
    template = 'users/member_list.html'
    return render(request, template, {'members': members})


class MemberListView(LoginRequiredMixin, ListView):
    model = Member
    context_object_name = 'members'


class MemberTypeListView(LoginRequiredMixin, ListView):
    model = Member
    template_name = 'users/member_type_list.html'


class AgentListView(LoginRequiredMixin, ListView):
    queryset = Member.members.agents()
    context_object_name = 'members'
    template_name = 'users/member_list.html'


class ConsumerListView(LoginRequiredMixin, ListView):
    queryset = Member.members.consumers()
    context_object_name = 'members'


class FarmerListView(LoginRequiredMixin, ListView):
    model = Member

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['members'] = Member.members.farmers()
        return context


class RetailerListView(LoginRequiredMixin, ListView):
    queryset = Member.members.retailers()
    context_object_name = 'members'


class WholesalerListView(LoginRequiredMixin, ListView):
    queryset = Member.members.wholesalers()
    context_object_name = 'members'
