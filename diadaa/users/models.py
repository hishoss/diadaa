from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Avg, F
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from .managers import MemberQuerySet
from .services import member_image_path
from .validators import phone_regex


@python_2_unicode_compatible
class User(AbstractUser):
    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_('Name of User'), blank=True, max_length=255)
    phone = models.CharField(validators=[phone_regex], max_length=17, blank=False,
                             verbose_name='Phone number', unique=True)

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('users:user_detail', kwargs={'username': self.username})


class Member(models.Model):
    REGION_CHOICES = (
        ('ar', 'Ashanti Region'),
        ('ba', 'Brong Ahafo Region'),
        ('cr', 'Central Region'),
        ('er', 'Eastern Region'),
        ('ga', 'Greater Accra'),
        ('nr', 'Northern Region'),
        ('ue', 'Upper East Region'),
        ('uw', 'Upper West Region'),
        ('vr', 'Volta Region'),
        ('wr', 'Western Region'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    user_image = models.ImageField(upload_to=member_image_path, blank=True)
    secondary_phone = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    region = models.CharField(max_length=3,  choices=REGION_CHOICES)
    location = models.CharField(max_length=100)
    is_agent = models.BooleanField(default=False, verbose_name='Agent')
    is_consumer = models.BooleanField(default=False, verbose_name='Consumer')
    is_farmer = models.BooleanField(default=False, verbose_name='Farmer')
    is_retailer = models.BooleanField(default=False, verbose_name='Retailer')
    is_wholesaler = models.BooleanField(default=False, verbose_name='Wholesaler')
    is_active = models.BooleanField(default=True)
    # is_new_member = models.BooleanField(default=True)

    members = MemberQuerySet.as_manager()

    def __str__(self):
        return self.user.username

    def average_rating(self):
        return self.user.rating.aggregate(
            average=Avg(F('communication') + F('honesty') + F('quality_of_goods')),
        )['average']

    def get_absolute_url(self):
        return reverse('users:member_detail',
                       kwargs={'username': self.user.username})

    def get_rating(self):
        # django_pandas seems to be struggling to convert numeric objects into int
        df = self.user.rating.to_dataframe()
        df = df[['communication', 'honesty', 'quality_of_goods']].astype(int)
        n = len(df)

        # we need to find the cumulative percentages i.e (total scores)/(total possible score) *100
        cum_percentages = (df.sum() / (5 * n)) * 100
        return cum_percentages.to_dict()

    def produce_names(self):
        return ", ".join(self.user.produce.values_list('name', flat=True))

    @property
    def loc_reg(self):
        return self.location, self.get_region_display()


# @receiver(post_save, sender=User)
# def create_member_profile(sender, instance, created, **kwargs):
#     if created:
#         Member.members.create(user=instance)


@receiver(post_save, sender=User)
def create_member_profile(**kwargs):
    if kwargs['created']:
        Member.members.create(user=kwargs['instance'])
