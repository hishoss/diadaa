from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from .models import User, Member


class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class MyUserCreationForm(UserCreationForm):

    error_message = UserCreationForm.error_messages.update({
        'duplicate_username': 'This username has already been taken.'
    })

    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


@admin.register(User)
class MyUserAdmin(AuthUserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    fieldsets = (
            ('User Profile', {'fields': ('name', 'phone')}),
    ) + AuthUserAdmin.fieldsets
    add_fieldsets = (
        (None, {'fields': ('username', 'password1', 'password2', 'phone')}),
    )
    list_display = ('username', 'name', 'is_superuser')
    search_fields = ['name', 'phone']


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    class Meta:
        model = Member

    # prepopulated_fields = {'slug': ('user',)}
    exclude = ('user',)
    list_display = ('user', 'location', 'average_rating')
    list_filter = ('region', 'is_agent', 'is_consumer', 'is_farmer', 'is_retailer',)
    search_fields = ('region',)

    def has_add_permission(self, request):
        return False


admin.site.site_title = 'diaDaa'
admin.site.site_header = 'diaDaa Administration'
