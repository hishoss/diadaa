from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.ProduceCollectionListView.as_view(), name='produce_collection_list'),
    # url(r'^$', views.ProduceListView.as_view(), name='produce_list'),
    url(r'^categories/$', views.CategoryListView.as_view(), name='category_list'),
    url(r'^categories/(?P<slug>[-\w]+/?)$', views.CategoryDetailView.as_view(), name='category_detail'),
    url(r'^add_produce$', views.produce_create_view, name='produce_create'),
    # url(r'^~(?P<slug>[-\w]+/?)$', views.produce_detail_view, name='produce_detail'),
    url(r'^(?P<name>[-\w]+/?)$', views.produce_collection_detail_view, name='produce_collection_detail'),
    url(r'^~(?P<slug>[-\w]+/?)$', views.ProduceDetailView.as_view(), name='produce_detail'),
    url(r'^~(?P<slug>[-\w]+/?)/update/$', views.ProduceUpdateView.as_view(), name='produce_update'),
]
