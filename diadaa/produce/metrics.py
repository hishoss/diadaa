from django.conf import settings
import googlemaps


gmaps = googlemaps.Client(key=settings.GOOGLE_MAPS_DISTANCE_MATRIX_API_KEY)

# from diadaa.produce.metrics import distance_duration, route_costs
# s = [('Madina', 'Greater Accra'), ('Ho', 'Volta Region')]
# d = ('Ayigya', 'Kumasi')
# route_costs(s,d)


# TODO: not just shortest, better computations will be added later, but we go for shortest now
def distance_duration(source, destination, units=None, gmaps=gmaps):
    """
    :param source: (location, region)
    :param destination: (location, region)  
    :param units: default Km
    :return: distance value, distance text, time value, time text
    """
    country = ", Ghana"
    result = gmaps.distance_matrix(source[0] + ", " + source[1] + country,
                                   destination[0] + ", " + destination[1] + country,
                                   units=units)
    d_text = result['rows'][0]['elements'][0]['distance']['text']
    d_value = result['rows'][0]['elements'][0]['distance']['value']
    t_text = result['rows'][0]['elements'][0]['duration']['text']
    t_value = result['rows'][0]['elements'][0]['duration']['value']
    return d_value, d_text, t_value, t_text
