from django.apps import AppConfig


class ProduceConfig(AppConfig):
    name = 'diadaa.produce'
    verbose_name = 'Produce'
