from django.contrib import admin
from .models import Category, Produce, ProduceCollection


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name',)}


class ProduceAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'category', 'price', 'stock', 'is_available','updated')
    list_filter = ('is_available', 'timestamp', 'updated', 'category')
    list_editable = ('price', 'stock', 'is_available')
    prepopulated_fields = {'slug': ('name', 'user')}


class ProduceCollectionAdmin(admin.ModelAdmin):
    list_filter = ('name',)
    list_display = ('name', 'produce_owners', 'owner_regions', 'min_avg_max', 'total_stock',
                    # 'locations',
                    )

admin.site.register(Category, CategoryAdmin)
admin.site.register(Produce, ProduceAdmin)
admin.site.register(ProduceCollection, ProduceCollectionAdmin)
