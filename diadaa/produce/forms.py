from django import forms
from django.forms.extras.widgets import SelectDateWidget

from .models import Produce


class ProduceForm(forms.ModelForm):
    class Meta:
        model = Produce
        fields = ('name', 'category', 'description', 'unit', 'price', 'stock',
                  'is_available', 'collection', 'date_from_source', 'availability')
        widgets = {'date_from_source': SelectDateWidget(),
                   'availability': SelectDateWidget()}

