def produce_directory_path(instance, filename):
    return 'member/{0}/produce/{1}'.format(instance.user.username, instance.slug)
