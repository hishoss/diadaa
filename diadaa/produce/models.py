from decimal import Decimal
from math import ceil

from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Avg, Max, Min

from diadaa.users.models import User

from .metrics import distance_duration
from .services import produce_directory_path


class Category(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True, unique=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('produce:category_detail', kwargs={'slug': self.slug})

    class Meta:
        ordering = ('name',)
        verbose_name = 'category'
        verbose_name_plural = 'categories'


class ProduceCollection(models.Model):
    name = models.CharField(max_length=100, unique=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    @property
    def image(self):
        # TODO: if there's nothing we need to return some blank image
        try:
            image = self.produce_set.exclude(image='')[0].image.url
        except:
            image = None
        return image

    def produce_owners(self):
        return ", ".join([p.user.username for p in self.produce_set.all()])

    def cost_from_location(self, destination):
        return distance_duration((self.user.member.location, self.user.member.region), destination)

    # we need to enforce a common unit else this isn't possible. we could also have different conversions
    @property
    def min_avg_max(self):
        return self.price_min, self.price_avg, self.price_max

    def closest_source_cost(self, destination, demand_qty, units=None, t_rate=0.0001, q_rate=0.005):
        # we may use weight rate rather than quantity rate
        # if demand_qty is less than closest source stock, then that'll be the cost, else we do something else
        # stock -> {id: (stock, location, region)}
        # we need {id: (stock, location, region, cost per unit item)
        # [(id, stock, location, region, distance value, distance text, time value, time text, transportation cost)]
        # TODO: we use an upper bound for the first on the cost assuming the second source might meet demand.
        # TODO: This is an optimization problem that needs to be solved. One might think a greedy answer is best because
        # TODO: you'll just need to go for the closest source but prices will also come into play and the rates of
        # TODO: moving the items

        d = sorted(
            [(k, *v, *distance_duration(v[1:], destination, units=units)) for k, v in self.__stock.items()],
            key=lambda x: x[4])
        if demand_qty < d[0][1]:
            return (*d[0], ceil(float(d[0][4])*t_rate + demand_qty*q_rate))
        else:
            return (*d[1], ceil(float(d[1][4])*t_rate + demand_qty*q_rate))

    @property
    def price(self):
        return self.price_max

    # def transportation_cost(self, source, quantity):
    #     # TODO: price will be based on several factors. 1st will be cost of transportation
    #     # TODO: we use the max for now
    #     # TODO: fictitious pricing. we might actually need the weights
    #
    #     cs = Decimal(self.closest_source(source, quantity)[4])
    #     quantity = Decimal(quantity)
    #     return Decimal('0.005')*cs * Decimal('0.1')*quantity

    @property
    def price_min(self):
        return self.produce_set.all().aggregate(r=Min('price'))['r'] or 0

    @property
    def price_avg(self):
        return self.produce_set.all().aggregate(r=Avg('price'))['r'] or 0

    @property
    def price_max(self):
        return self.produce_set.all().aggregate(r=Max('price'))['r'] or 0

    def get_absolute_url(self):
        return reverse('produce:produce_collection_detail', kwargs={'name': self.name})

    @property
    def __stock(self):
        # we need to know the stock from each location
        return {p.user_id: (p.stock, p.user.member.location, p.user.member.get_region_display())
                for p in self.produce_set.all()}

    @property
    def total_stock(self):
        return sum([s[0] for s in self.__stock.values()])

    @property
    def unit(self):
        # TODO: we need to have a unit-unit conversion matrix
        # TODO: for now I'm gonna use the first unit. then we could then be able to switch to the desired units
        try:
            unit = self.produce_set.first().get_unit_display()
        except:
            unit = None
        return unit

    @property
    def owner_regions(self):
        return ", ".join([p.user.member.get_region_display()[:-7] for p in self.produce_set.all()])


class Produce(models.Model):
    UNIT_CHOICES = (
        ('bg', 'Bag'),
        ('bs', 'Basket'),
        ('bx', 'Box'),
        ('co', 'Container'),
        ('cp', 'Cups'),
        ('cr', 'Crate'),
        ('g', 'Gram'),
        ('gl', 'Gallon'),
        ('kg', 'Kilogram'),
        ('lb', 'Pounds'),
        ('Nm', 'Number'),
        ('ol', 'Olonka'),
        ('O', 'Other')
    )
    user = models.ForeignKey(User, related_name='produce', on_delete=models.CASCADE)
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True, unique=True)
    category = models.ForeignKey(Category)
    collection = models.ForeignKey(ProduceCollection)
    image = models.ImageField(upload_to=produce_directory_path, blank=True)
    description = models.CharField(max_length=100)
    # standard unit and non standard unit
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Price per unit GHS')
    unit = models.CharField(max_length=3, choices=UNIT_CHOICES)
    stock = models.PositiveIntegerField()
    date_from_source = models.DateField()
    is_available = models.BooleanField(default=True)
    availability = models.DateField(verbose_name="Available from")
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-timestamp',)
        index_together = (('id', 'slug'),)
        verbose_name_plural = "produce"

    def __str__(self):
        return self.name + ' from ' + self.user.username

    def get_absolute_url(self):
        return reverse('produce:produce_detail', kwargs={'slug': self.slug})
