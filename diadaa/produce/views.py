from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.utils import timezone
from django.utils.text import slugify
from django.views.decorators.http import require_POST
from django.views.generic import DetailView, ListView, UpdateView

from diadaa.analytics.mixins import ObjectViewedMixin
from diadaa.analytics.signals import object_viewed_signal
from diadaa.cart.forms import CartAddProduceForm

from .forms import ProduceForm
from .models import Category, Produce, ProduceCollection


class CategoryDetailView(LoginRequiredMixin, DetailView, ObjectViewedMixin):
    model = Category
    slug_field = 'slug'
    slug_url_kwarg = 'slug'
    template_name = 'category/category_detail.html'
#
#     def get_object(self, **kwargs):
#         instance = super(CategoryDetailView, self).get_object(**kwargs)
#         object_viewed_signal.send(Category, instance=self.object, request=self.kwargs['request'])
#         return get_object_or_404(Category, slug=self.kwargs.get('slug', None))


class CategoryListView(ListView):
    model = Category
    template_name = 'category/category_list.html'
    context_object_name = 'categories'


# def category_detail_view(request, slug):
#     template = 'category/category_detail.html'
#     category = get_object_or_404(Category, slug=slug)
#     object_viewed_signal.send(Category, instance=category, request=request)
#     return render(request, template, {'category': category})


# class ProduceListView(ListView):
#     model = Produce
#     template_name = 'produce/produce_list.html'
#     context_object_name = 'produce'


class ProduceDetailView(LoginRequiredMixin, DetailView, ObjectViewedMixin):
    model = Produce
    slug_field = 'slug'
    slug_url_kwarg = 'slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cart_produce_form'] = CartAddProduceForm()
        context['produce'] = get_object_or_404(Produce, slug=self.kwargs.get('slug', None), is_available=True)
        return context


@login_required
def produce_detail_view(request, slug):
    template = 'produce/produce_detail.html'
    produce = get_object_or_404(Produce, slug=slug, is_available=True)
    object_viewed_signal.send(Produce, instance=produce, request=request)
    cart_produce_form = CartAddProduceForm()
    return render(request, template, {'produce': produce,
                                      'cart_produce_form': cart_produce_form})


class ProduceCollectionListView(ListView):
    model = ProduceCollection
    template_name = 'produce/produce_collection_list.html'
    context_object_name = 'produce'


@login_required
def produce_collection_detail_view(request, name):
    template = 'produce/produce_collection_detail.html'
    produce = get_object_or_404(ProduceCollection, name=name)
    object_viewed_signal.send(ProduceCollection, instance=produce, request=request)
    cart_produce_form = CartAddProduceForm()
    return render(request, template, {'produce': produce,
                                      'cart_produce_form': cart_produce_form})


# @require_POST
@login_required
def produce_create_view(request):
    form = ProduceForm(request.POST or None)
    if form.is_valid():
        produce_obj = form.save(commit=False)
        produce_obj.user = request.user
        produce_obj.slug = slugify(produce_obj.name, request.user.id)
        # quick fix. Need a way to use a date picker
        # checkout https://github.com/pbucher/django-bootstrap-datepicker
        # produce_obj.date_from_source = timezone.now().date()
        # produce_obj.availability = timezone.now().date()
        produce_obj.save()
        return HttpResponseRedirect('/users/{id}'.format(id=request.user.id))
    return render(request, 'produce/produce_create.html', {'form': form})


class ProduceUpdateView(LoginRequiredMixin, UpdateView):
    model = Produce
    fields = ('name', 'category', 'description', 'unit', 'price', 'availability',)
    template_name = 'produce/produce_form.html'

    def get_object(self, **kwargs):
        return get_object_or_404(Produce, slug=self.kwargs.get('slug', None))

    def get_success_url(self):
        return reverse('produce:produce_detail',
                       kwargs={'slug': self.kwargs['slug']})
