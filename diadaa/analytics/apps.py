from django.apps import AppConfig


class AnalyticsConfig(AppConfig):
    name = 'diadaa.analytics'
    verbose_name = 'Analytics'
