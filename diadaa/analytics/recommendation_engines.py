# from django.contrib.contenttypes.models import ContentType

from diadaa.users.models import User
from diadaa.produce.models import Produce
from diadaa.analytics.models import ObjectViewed, UserCluster

from scipy.sparse import dok_matrix
from sklearn.cluster import KMeans
import numpy as np


# p_id = ContentType.objects.get(app_label='diadaa.produce', model='produce').id
p_id = 14


def naive_produce_recommendation_list(user_id):
    user_produce = ObjectViewed.objects.filter(user_id=user_id)  # .prefetch_related('content_object')
    # produce has content_type_id == 14
    user_produce_ids = {x.object_id for x in user_produce if x.content_type_id == p_id}
    return Produce.objects.exclude(id__in=user_produce_ids)


def update_user_clusters():
    num_views = ObjectViewed.objects.count()
    update_step = ((num_views/100)+1) * 5

    # NEEDS TO BE == 0 but can't figure how to fix this now
    # using some magic numbers here, sorry...
    if num_views % update_step != 0:
        all_user_ids = User.objects.only("id").values_list('id', flat=True).order_by('id')
        all_produce_ids = Produce.objects.only("id").values_list('id', flat=True).order_by('id')
        all_produce_viewed = ObjectViewed.objects.filter(content_type_id=p_id).all()
        num_users = len(all_user_ids)
        num_produce = len(all_produce_ids)
        views_m = dok_matrix((num_users, num_produce), dtype=np.float32)

        # for each user(row) fill each produce with 1 if viewed, 0 otherwise
        for u in all_user_ids:
            user_produce_viewed_ids = set(all_produce_viewed.filter(user_id=u).values_list('object_id', flat=True))
            for produce_viewed in user_produce_viewed_ids:
                views_m[u-1, produce_viewed-1] = 1

        # Perform kmeans clustering
        k = int(num_users / 10) + 2
        kmeans = KMeans(n_clusters=k)
        clustering = kmeans.fit(views_m.tocsr())

        # Update clusters
        UserCluster.objects.all().delete()
        new_clusters = {i: UserCluster(name=str(i)) for i in range(k)}
        # map(lambda c: c.save, new_clusters.values())
        for cluster in new_clusters.values():  # clusters need to be saved before referring to users
            cluster.save()
        for u, cluster_label in enumerate(clustering.labels_):
            new_clusters[cluster_label].users.add(User.objects.get(id=u+1))


def produce_recommendation_list(user_id, produce_content_type_id=p_id):
    # need to query better for the object type cos the content_type_id changes when database is reset
    all_produce_viewed = ObjectViewed.objects.filter(content_type_id=produce_content_type_id).all()

    if not all_produce_viewed:
        return []  # nothing to do

    # still dunno why I need to order_by before distinct works
    user_viewed_produce_ids = \
        all_produce_viewed.filter(user_id=user_id  # produce has content_type_id == 14
                                  ).values_list('object_id', flat=True).distinct().order_by()

    # user_produce_ids = Member.members.get(id=user_id).produce().values_list('id', flat=True)
    user_produce_ids = User.objects.get(id=user_id).produce.values_list('id', flat=True)

    # get request user cluster name (just the first right now)
    try:
        user_cluster_id = User.objects.get(id=user_id).usercluster_set.first()
        user_cluster_id = user_cluster_id.id
    except AttributeError:
        update_user_clusters()
        user_cluster_id = User.objects.get(id=user_id).usercluster_set.first().id

    # get user ids for other members of the cluster
    user_cluster_other_members_ids = UserCluster.objects.get(id=user_cluster_id).users.exclude(id=user_id).all()

    # exclude produce already seen by user or in user owns
    unseen_produce_ids = set(user_viewed_produce_ids) | set(user_produce_ids)
    other_users_in_cluster_viewed_produce_ids = \
        all_produce_viewed.filter(user_id__in=user_cluster_other_members_ids
                                  ).exclude(object_id__in=unseen_produce_ids
                                            ).values_list('object_id', flat=True)
    other_users_in_cluster_viewed_produce_ids = set(other_users_in_cluster_viewed_produce_ids)

    # sort this list based on price?
    produce_list = sorted(
        Produce.objects.filter(id__in=other_users_in_cluster_viewed_produce_ids),
        key=lambda p: p.price
    )
    return produce_list


#
#
# class RecommendationEngine:
#     def __init__(self, content_type_name, user_id, user_model=User):
#         self.content_type_name = content_type_name
#         self.user_id = user_id
#         self.user_model = user_model
#         # self.content_type_id = ContentType.objects.get(app_label='produce', model='produce').id
#         # we need to try this as it may be unavailable
#         self.content_type_object = ContentType.objects.get(model=content_type_name)
#         self.content_type_id = self.content_type_object.id
#
#     def get_object_ids(self):
#         objects = ObjectViewed.objects.filter(content_type_id=self.content_type_id).all()
#         if objects:
#             # still dunno why I need to order_by before distinct works
#             return objects.filter(user_id=self.user_id).values_list('object_id', flat=True).distinct().order_by()
#         return []  # no objects available
#
#     def get_user_object_ids(self):
#         objects = self.get_object_ids()
#         if objects:
#             # user_object_ids = self.user_model.objects.get(id=self.user_id).produce.values_list('id', flat=True)
#             user_object_ids = self.user_model.objects.get(id=self.user_id).get(
#                 self.content_type_name).values_list('id', flat=True)
#             return set(user_object_ids)
#         return []
#
#     def get_user_cluster(self):
#         # get request user cluster name (just the first right now)
#         try:
#             user_cluster_id = self.user_model.objects.get(id=self.user_id).usercluster_set.first()
#             user_cluster_id = user_cluster_id.id
#         except AttributeError:
#             update_user_clusters()
#             user_cluster_id = self.user_model.objects.get(id=self.user_id).usercluster_set.first().id
#
#         # get user ids for other members of the cluster
#         user_cluster_other_members_ids = UserCluster.objects.get(
#             id=user_cluster_id).users.exclude(id=self.user_id).all()
#
#     def get_recommendation_list(self):
#         # exclude produce already seen by user or in user owns
#         user_objects_complement_ids = set(user_viewed_produce_ids) | self.get_user_object_ids()
#         other_users_in_cluster_viewed_produce_ids = \
#             all_produce_viewed.filter(user_id__in=user_cluster_other_members_ids
#                                       ).exclude(object_id__in=unseen_produce_ids
#                                                 ).values_list('object_id', flat=True)
#         other_users_in_cluster_viewed_produce_ids = set(other_users_in_cluster_viewed_produce_ids)
#
#         # sort this list based on price?
#         produce_list = sorted(
#             Produce.objects.filter(id__in=other_users_in_cluster_viewed_produce_ids),
#             key=lambda p: p.price
#         )
#         return produce_list
