from django.shortcuts import render, redirect
from django.views.generic import ListView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from diadaa.produce.models import Produce
from diadaa.logistics.models import Inventory
from diadaa.users.models import User, Member

from .charts import GenericChart, StackedBarChart
from .models import ObjectViewed


class HomeView(ListView, LoginRequiredMixin):
    queryset = User.objects.all().order_by('username')
    template_name = 'analytics/home.html'
    context_object_name = 'users'


@login_required
def home_view(request):
    template = 'analytics/home.html'
    context = {'users': User.objects.all().order_by('username')}
    return render(request, template, context)


@login_required
def user_dashboard(request, username):
    if not request.user.is_staff:
        return redirect('/403')

    user_id = User.objects.get(username=username).id
    context = {'produce_chart': None, 'produce_title': '!', 'produce_info': "This user hasn't viewed anything yet",
               'produce_caption': 'produce caption', 'rating_chart': None, 'rating_title': '!',
               'rating_info': 'This user has no rating', 'rating_caption': 'rating caption'}

    produce_viewed = ObjectViewed.objects.filter(user_id=user_id)
    if produce_viewed:
        freq = dict(produce_viewed.to_dataframe()['object_id'].value_counts())
        # this step to prevent duplicates since there could be same item from different users
        freq = {Produce.objects.get(id=obj_id).name: counts for obj_id, counts in freq.items()}.items()
        labels = [x[0] for x in freq]
        # use .item() on numpy integers to convert to native numbers else it won't be JSON serializable
        data = [x[1].item() for x in freq]
        produce_chart = GenericChart(
            data=data,
            labels=labels
        )
        produce_chart.chart_type = 'polarArea'
        context['produce_chart'] = produce_chart
        context['produce_title'] = 'items viewed by ' + username
        context['produce_info'] = 'produce description'
        context['produce_caption'] = 'produce caption'

    user_rating = Member.members.get(user_id=user_id).get_rating()

    # empty model entries are nan, not 'nan' and it's annoying
    if sum(user_rating.values()) >= 0:
        rating_chart = GenericChart(
            labels=list(user_rating.keys()),
            data=list(user_rating.values())
        )
        rating_chart.chart_type = 'bar'
        context['rating_chart'] = rating_chart
        context['rating_title'] = username + ' is rated as follows'
        context['rating_info'] = 'rating description'
        context['rating_caption'] = 'rating caption'

    # can we have plots for both demand and stock on the same graph
    inventory = Inventory.objects.all().distinct('produce_id')
    data, labels = [], []
    for produce in inventory:
        labels += [produce.produce.name]
        data += [produce.total_demand]
    inventory_chart = StackedBarChart(
        data=data,
        labels=labels
    )
    inventory_chart.chart_type = 'bar'
    context['inventory_chart'] = inventory_chart
    context['inventory_info'] = 'lala'
    context['inventory_caption'] = 'qaqa'

    return render(request, 'analytics/dashboard.html', context)
