from django import template

from ..charts import GenericChart

register = template.Library()


@register.inclusion_tag('analytics/chart_card.html')
def show_chart(username, data, labels, chart_type):
    context = {'chart': None, 'title': '!', 'info': "This user hasn't viewed anything yet",
               'caption': 'caption'}
    chart = GenericChart(
        data=data,
        labels=labels
    )
    chart.chart_type = chart_type
    context['chart'] = chart
    context['title'] = 'items viewed by ' + username
    pass
