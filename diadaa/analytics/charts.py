from jchart import Chart
from jchart.config import Axes, DataSet, rgba
from random import randint


def rand_color(n):
    return [rgba(randint(0, 220), randint(0, 220), randint(0, 220), 0.6)
            for _ in range(n)]


class GenericChart(Chart):
    chart_type = 'bar'
    animation = {'duration': 2000}
    scales = {'yAxes': [{'ticks': {'beginAtZero': True}}]}

    def __init__(self, labels, data):
        self.labels = labels
        self.data = data
        super(GenericChart, self).__init__()

    def get_labels(self):
        return self.labels

    def get_datasets(self):
        colors = rand_color(len(self.data))
        return [
            DataSet(label='',
                    data=self.data,
                    borderWidth=3,
                    backgroundColor=colors,
                    borderColor=colors)
        ]


class StackedBarChart(Chart):
    chart_type = 'bar'
    animation = {'duration': 2000}
    scales = {'yAxes': [{'ticks': {'beginAtZero': True},
                        'stacked': True}],
              'xAxes': [{'stacked': True}]
              }

    def __init__(self, labels, data):
        self.labels = labels
        self.data = data
        super(StackedBarChart, self).__init__()

    def get_labels(self):
        return self.labels

    def get_datasets(self):
        colors = rand_color(len(self.data))
        return [
            DataSet(label='Chart',
                    data=self.data,
                    borderWidth=3,
                    backgroundColor=colors,
                    borderColor=colors)
        ]
