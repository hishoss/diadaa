from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django_pandas.managers import DataFrameManager

from diadaa.users.models import User

from .utils import get_client_ip
from .signals import object_viewed_signal


class ObjectViewed(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)  # User instance instance.id
    ip_address = models.CharField(max_length=220, blank=True, null=True)  # IP Field
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    timestamp = models.DateTimeField(auto_now_add=True)

    objects = DataFrameManager()  # for to_dataframe, to_timeseries, to_pivot_table

    def __str__(self, ):
        return "{!s} viewed {!s} at {!s}".format(self.user.username, self.content_object, self.timestamp)

    class Meta:
        ordering = ['-timestamp'] # most recent saved show up first
        verbose_name = 'Object viewed'
        verbose_name_plural = 'Objects viewed'


def object_viewed_receiver(sender, instance, request, *args, **kwargs):
    c_type = ContentType.objects.get_for_model(sender)  # instance.__class__
    ObjectViewed.objects.create(
                user=request.user,
                content_type=c_type,
                object_id=instance.id,
                ip_address=get_client_ip(request)
        )


object_viewed_signal.connect(object_viewed_receiver)


class UserRating(models.Model):
    RATING_CHOICES = (
      (0, '0'),
      (1, '1'),
      (2, '2'),
      (3, '3'),
      (4, '4'),
      (5, '5'),
    )
    user = models.ForeignKey(User, related_name='rating')
    communication = models.IntegerField(choices=RATING_CHOICES, default=0)
    honesty = models.IntegerField(choices=RATING_CHOICES, default=0)
    quality_of_goods = models.IntegerField(choices=RATING_CHOICES, default=0)
    comment = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)  # we can see how rating progresses

    objects = DataFrameManager()

    def __str__(self):
        return self.user.username


class UserCluster(models.Model):
    name = models.CharField(max_length=100)
    users = models.ManyToManyField(User)
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def get_members(self):
        return ", ".join([u.username for u in self.users.all()])

    def __str__(self):
        return self.name

