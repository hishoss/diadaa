from django.views.generic.detail import SingleObjectMixin
from .signals import object_viewed_signal


class ObjectViewedMixin(SingleObjectMixin):
    def get_object(self, queryset=None):
        instance = super(ObjectViewedMixin, self).get_object(queryset)
        if instance:
            object_viewed_signal.send(instance.__class__, instance=instance, request=self.request)
        return instance
