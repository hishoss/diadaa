from jchart import Chart
from jchart.config import Axes, DataSet, rgba


class ExampleRadarChart(Chart):
    chart_type = 'radar'

    def get_labels(self):
        return ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"]

    def get_datasets(self, **kwargs):
        return [DataSet(label="My First dataset",
                        color=(179, 181, 198),
                        data=[65, 59, 90, 81, 56, 55, 40]),
                DataSet(label="My Second dataset",
                        color=(255, 99, 132),
                        data=[28, 48, 40, 19, 96, 27, 100])
                ]


class ExamplePolarChart(Chart):
    chart_type = 'polarArea'

    def get_labels(self, **kwargs):
        return ["Red", "Green", "Yellow", "Grey", "Blue"]

    def get_datasets(self, **kwargs):
        return [DataSet(label="My DataSet",
                        data=[11, 16, 7, 3, 14],
                        backgroundColor=[
                            "#FF6384",
                            "#4BC0C0",
                            "#FFCE56",
                            "#E7E9ED",
                            "#36A2EB"
                        ])
                ]


class ExampleBarChart(Chart):
    chart_type = 'bar'
    animation = {'duration': 2000}
    scales = {'yAxes': [{'ticks': {'beginAtZero': True }}]}

    def get_labels(self, **kwargs):
        return ["Min", "Mean", "Max"]

    def get_datasets(self, **kwargs):
        data = [15, 20, 25]
        colors = [
            rgba(255, 99, 132, 0.2),
            rgba(54, 162, 235, 0.2),
            rgba(255, 206, 86, 0.2),
        ]

        return [DataSet(label='Bar Chart',
                        data=data,
                        borderWidth=3,
                        backgroundColor=colors,
                        borderColor=colors)]


class ExampleBubbleChart(Chart):
    chart_type = 'bubble'

    def get_datasets(self, **kwargs):
        data = [{
            'x': randint(1, 10),
            'y': randint(1, 25),
            'r': randint(1, 10),
        } for i in range(25)]

        return [DataSet(label="First DataSet",
                        data=data,
                        backgroundColor='#FF6384',
                        hoverBackgroundColor='#FF6384')]

