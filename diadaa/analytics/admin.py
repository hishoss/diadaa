from django.contrib import admin

from .models import UserRating, ObjectViewed, UserCluster


class ObjectViewedAdmin(admin.ModelAdmin):
    list_per_page = 20

    def has_add_permission(self, request):
        return False

    # def has_change_permission(self, request):
    #     return False


class UserRatingAdmin(admin.ModelAdmin):
    list_display = ('user', 'honesty', 'communication', 'quality_of_goods')

    class Meta:
        ordering = ('-timestamp',)


class UserClusterAdmin(admin.ModelAdmin):
    list_display = ['name', 'get_members']


admin.site.register(ObjectViewed, ObjectViewedAdmin)
admin.site.register(UserRating, UserRatingAdmin)
admin.site.register(UserCluster, UserClusterAdmin)
