from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.home_view, name='home'),
    url(r'^dashboard/(?P<username>[\w.@+-]+)/$', views.user_dashboard, name='dashboard'),
]
