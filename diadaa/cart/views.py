from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from diadaa.produce.models import Produce, ProduceCollection
from .cart import Cart
from .forms import CartAddProduceForm


@require_POST
def cart_add(request, produce_id):
    produce = get_object_or_404(ProduceCollection, id=produce_id)
    cart = Cart(request)
    form = CartAddProduceForm(request.POST or None)
    if form.is_valid():
        cd = form.cleaned_data
        cart.add(produce=produce,
                 quantity=cd['quantity'],
                 update_quantity=cd['quantity'])
    return redirect('cart:cart_detail')


def cart_remove(request, produce_id):
    produce = get_object_or_404(ProduceCollection, id=produce_id)
    cart = Cart(request)
    cart.remove(produce)
    return redirect('cart:cart_detail')


def cart_detail(request):
    cart = Cart(request)
    for item in cart:
        item['update_quantity_form'] = CartAddProduceForm(initial={'quantity': item['quantity'], 'update': True})
    return render(request, 'cart/cart_detail.html', {'cart': cart})
