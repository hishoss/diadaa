from django.apps import AppConfig


class CartConfig(AppConfig):
    name = 'diadaa.cart'
    verbose_name = 'Cart'
