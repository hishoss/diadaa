from decimal import Decimal, getcontext
from django.conf import settings
from diadaa.produce.models import ProduceCollection


class Cart:
    def __init__(self, request):
        """
        initialise the cart.
        """
        self.session = request.session
        cart = self.session.get(settings.CART_SESSION_ID)
        if not cart:
            # save an empty cart in the session
            cart = self.session[settings.CART_SESSION_ID] = {}
        self.cart = cart
        self.request = request

    def add(self, produce, quantity=1, update_quantity=False):
        """
        Add a produce to the cart or update its quantity
        :param produce: an instance of the ProduceCollection model.
        :param quantity: an integer to indicate how much quantity is gonna be added.
        :param update_quantity: boolean value to indicate an update in quantity(True) or just add a unit(False)
        :return: None, but sessions key 'cart' with produce.id will be updated
        """
        produce_id = str(produce.id)
        if produce_id not in self.cart:
            self.cart[produce_id] = {'quantity': 0}
        if update_quantity:
            self.cart[produce_id]['quantity'] = quantity
        else:
            self.cart[produce_id]['quantity'] += quantity
        source = produce.closest_source_cost(self.request.user.member.loc_reg,
                                             self.cart[produce_id]['quantity'])

        self.cart[produce_id]['price'] = str(produce.price)
        self.cart[produce_id]['closest_source_location'] = str(source[2] + ", " + source[3])
        self.cart[produce_id]['closest_source_distance'] = str(source[5])
        self.cart[produce_id]['transportation_cost'] = str(source[-1])
        self.save()

    def save(self):
        # update the session cart
        self.session[settings.CART_SESSION_ID] = self.cart
        # mark the session as "modified" to make sure it is saved
        self.session.modified = True

    def remove(self, produce):
        # Remove a produce from the cart
        produce_id = str(produce.id)
        if produce_id in self.cart:
            del self.cart[produce_id]
            self.save()

    def __iter__(self):
        """
        Iterate over the items in the cart and get the produce from the database.
        :return: 
        """
        produce_ids = self.cart.keys()
        # get the produce objects and add them to the cart
        produce_objs = ProduceCollection.objects.filter(id__in=produce_ids)
        for produce in produce_objs:
            self.cart[str(produce.id)]['produce'] = produce

        for item in self.cart.values():
            item['price'] = Decimal(item['price'])
            item['cost_of_goods'] = item['price'] * item['quantity']
            item['total_cost'] = item['cost_of_goods'] + Decimal(item['transportation_cost'])
            yield item

    def __len__(self):
        """Return number of items in cart"""
        return sum(item['quantity'] for item in self.cart.values())

    def get_total_cost(self):
        """Return total price of items in cart"""
        return str(sum(Decimal(item['price']) * item['quantity'] + Decimal(item['transportation_cost'])
                   for item in self.cart.values()))

    def clear(self):
        # remove cart from session
        del self.session[settings.CART_SESSION_ID]
        self.session.modified = True
